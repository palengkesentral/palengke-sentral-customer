import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Network } from '@ionic-native/network';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ApiProvider } from '../providers/api/api';
import { GlobalsProvider } from '../providers/globals/globals';
import { HttpClientModule } from '@angular/common/http';
import { ToastMessagesProvider } from '../providers/toast-messages/toast-messages';
import { IonicStorageModule } from '@ionic/storage';
import { AlertMessageProvider } from '../providers/alert-message/alert-message';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Facebook } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { Toast } from '@ionic-native/toast';

import { ResponseInterceptorModule } from './response-interceptor.module';
import { HeaderInterceptorModule } from './header-interceptor.module';


@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      // mode: 'md'
    }),
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['sqlite', 'websql', 'indexeddb']
    }),
    // HeaderInterceptorModule,
    // ResponseInterceptorModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiProvider,
    InAppBrowser,
    GlobalsProvider,
    Network,
    Facebook,
    GooglePlus,
    Toast,
    {
      provide: ErrorHandler, useClass: IonicErrorHandler
    },
    ToastMessagesProvider,
    AlertMessageProvider,
  ]
})
export class AppModule { }
