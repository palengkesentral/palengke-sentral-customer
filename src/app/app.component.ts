import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, App, IonicApp, Nav, Events, Config } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Network } from '@ionic-native/network';
import { ToastMessagesProvider } from '../providers/toast-messages/toast-messages';
import { Storage } from '@ionic/storage';
import { ThrowStmt } from '@angular/compiler';
import { ApiProvider } from '../providers/api/api';
import { AlertMessageProvider } from '../providers/alert-message/alert-message';
import { Toast } from '@ionic-native/toast';
import { GlobalsProvider } from '../providers/globals/globals';
import { Facebook } from '@ionic-native/facebook';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';



export enum ConnectionStatus {
  Online,
  // Connecting,
  Offline
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  counter: number = 0

  previousStatus: any;
  userInfo: any;
  userFb: any;

  loginType: any;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private menuController: MenuController,
    private appCtrl: App,
    private ionicApp: IonicApp,
    private toastMsg: ToastMessagesProvider,
    private toast: Toast,
    private network: Network,
    private storage: Storage,
    private api: ApiProvider,
    private events: Events,
    private alertMessage: AlertMessageProvider,
    private config: Config,
    private global: GlobalsProvider,
    private fb: Facebook,
    private iab: InAppBrowser
  ) {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleLightContent()
      this.splashScreen.hide();
      this.checkIntroShown()
      this.config.set('backButtonIcon', 'custom-icon-left');
      this.initializeNetworkEvents()
      this.hardwareBackButton()
      this.checkCart()
      this.checkGlobalValue()
    });


    this.events.subscribe('isLoggedIn', () => {
      console.log('userevent');
      this.checkToken()
    });

    this.events.subscribe('refreshUserInfo', () => {
      this.checkToken()
    })

    this.events.subscribe('isFbLoggedIn', (data) => {
      this.userFb = data
      this.storage.set('fb_info', data)
    })
  }

  // ngOnInit() {
  //   this.checkCart()

  // }

  checkRefreshAnnouncement() {
    
  }

  ionViewWillLeave() {
    this.events.unsubscribe('isLoggedIn')
    this.events.unsubscribe('refreshUserInfo')
    this.events.unsubscribe('isFbLoggedIn')
  }

  async checkGlobalValue() {
    this.global.cart = await this.storage.get('cart')
    this.global.totalAmount = await this.storage.get('totalAmount')


  }

  async checkIntroShown() {
    const res: any = await this.storage.get('introShown')
    if (res) {
      this.checkToken()
    } else {
      this.rootPage = 'WalkthroughPage'
    }
  }

  async checkToken() {
    const res: any = await this.storage.get('token')
    if (res) {
      this.rootPage = 'TabsPage'
      this.getMyInfo()
    } else {
      this.rootPage = 'LoginPage'
    }
  }


  initializeNetworkEvents() {
    this.network.onDisconnect().subscribe(() => {
      if (this.previousStatus === ConnectionStatus.Online) this.toastMsg.detectConnection("You're Offline. No internet connection", null, 'noInternetConnectionToast');
      // this.events.publish('network:disconnected')
      console.log('Offline')
      this.previousStatus = ConnectionStatus.Offline;
    });

    this.network.onConnect().subscribe(() => {
      console.log('Online')
      this.toastMsg.dismissConnectionToast();
      // this.events.publish('network:connected')
      this.toastMsg.detectConnection("You're now Online", 3000, 'SuccessToast')
      // if (!this.storage.get('playerId')) {
      //   this.oneSignalProvider.init()
      // }
      this.previousStatus = ConnectionStatus.Online;
    });

    if (this.network.type === 'none') {
      console.log('No connection detected')
      // this.events.publish('network:disconnected')
      this.toastMsg.detectConnection("You're Offline. No internet connection", null, 'noInternetConnectionToast');
    }
  }


  hardwareBackButton() { // hardware back button handler to prevent quitting the app (for Android only)
    this.platform.registerBackButtonAction(() => { // try to dismiss any popup / modal / pushed pages /overlay component
      let activePortal = this.ionicApp._loadingPortal.getActive() ||
        this.ionicApp._modalPortal.getActive() ||
        this.ionicApp._overlayPortal.getActive()

      if (activePortal) {
        activePortal.dismiss()  // back button will trigger when there's an open portal
        return;
      }

      if (this.menuController.isOpen()) {
        this.menuController.close()  // back button will trigger when there's an open menu
        return;
      }

      else if (this.nav.canGoBack()) {
        this.nav.pop() // back button will trigger when there's an open poppable page
      }

      else if (this.appCtrl.getActiveNav().canGoBack()) {
        this.appCtrl.navPop() // back button will trigger when there's overlay component 
      }

      else if (this.appCtrl.getActiveNav()) {
        console.log('Get Nav', this.appCtrl.getActiveNav());
        this.appCtrl.navPop()  // back button will trigger when there is an open poppable page inside a tab/nav
        if (this.counter == 0) {
          this.counter++
          setTimeout(() => { this.counter = 0 }, 2000)
          this.presentToast('Press again to exit')
        } else {
          this.platform.exitApp() // if the user clicked the back button (hardware) before 2 seconds, it will exit the app
        }
        return
      }
    });
  }

  async goToPage(page) {
    this.appCtrl.getActiveNavs()[0].push(page)
  }

  presentToast(msg) {
    this.toast.showWithOptions({
      message: msg,
      duration: 3000,
      position: "bottom",
      styling: {
        backgroundColor: '#1E1E1F',
        textColor: '#FCFCFF',
      }
    }).subscribe(toast => { console.log(toast); });
  }

  async logOut(page) {
    const res: any = await this.storage.get('loginType')

    if (res === 'fb') {
      await this.fb.logout()
      await this.storage.set('totalAmount', 0)
      await this.storage.set('cart', [])
      this.global.cart = []
      this.global.totalAmount = 0
    }

    await this.storage.set('totalAmount', 0)
    await this.storage.set('cart', [])
    this.global.cart = []
    this.global.totalAmount = 0
    this.storage.remove('fbInfo')
    this.storage.remove('fb_info')
    this.storage.remove('user_id')
    this.storage.remove('token')
    this.storage.remove('loginType')
    this.nav.setRoot(page)

  }

  async getMyInfo() {
    this.loginType = await this.storage.get('loginType')
    const res: any = await this.api.getMe()
    if (this.loginType === 'fb') {
      this.storage.set('user_id', res)
      const info = await this.storage.get('fb_info')
      this.userFb = info
      console.log(this.userFb);
    } else {
      try {
        this.storage.set('user_id', res)
        res.m_avatar = `https:${res.m_avatar}`
        this.userInfo = res
        console.log(res);
        if (res.message === 'Expired token') {
          this.logOut('LoginPage')
        }
      } catch (error) {
        console.log(error);
        if (error.message === 'Expired token') {
          this.logOut('LoginPage')
        }
      }
    }

  }

  async checkCart() {
    const cart = await this.storage.get('cart')
    const total = await this.storage.get('totalAmount')
    this.events.publish('addedToCart', cart)

    if (!cart) this.storage.set('cart', [])
    if (!total) this.storage.set('totalAmount', 0)
  }

  underConstruction() {
    this.alertMessage.open('Coming Soon', 'Thank you for your patience. ')
  }

  goToContactUs() {
    this.iab.create('https://palengkesentral.com/contact-us/')
  }
}
