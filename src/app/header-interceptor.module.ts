
import { HTTP_INTERCEPTORS, HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core'
import { IonicStorageModule, Storage } from '@ionic/storage';

// rxjs
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { fromPromise } from 'rxjs/observable/fromPromise';
import { switchMap } from 'rxjs/operators';




// import { ObjectConverterProvider } from '../exports/provider.exporter'; // uncomment if needed

@Injectable()
export class HttpsRequestInterceptor implements HttpInterceptor {

    token: any

    constructor(private storage: Storage) { }
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const re: any = ['https://palengkesentral.com/wp-json/jwt-auth/v1/token', '/products']; // add route that doesnt need a token request,
        // if multiple routes is needed, use an array and search using 'array.includes'
        return fromPromise(this.storage.get('token')).pipe(switchMap(token => { //
            if (!re.includes(req.url)) { // if const re is an array, use 're.includes'
                const headers = new HttpHeaders({
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json', // uncomment if needed
                    // 'Accept': 'application/x.full-time-win.v1+json',
                    // 'Accept-Language': localStorage.getItem('language') || 'en'
                })
                const dupReq = req.clone({ headers });
                return next.handle(dupReq);
            } else {
                return next.handle(req)
            }
        }))
        // return fromPromise(this.storage.get('token')).pipe(switchMap(token => {
        //     this.token = token
        //     const dupReq = req.clone({ headers: this.addExtraHeaders(req.headers) });
        //     return next.handle(dupReq);
        // }))

    }

    // private addExtraHeaders(headers: HttpHeaders): HttpHeaders {
    //     headers = headers.append('Authorization', `Bearer ${this.token}`)
    //     headers = headers.append('Content-Type', 'application/json');
    //     return headers;
    // }


};
@NgModule({
    imports: [IonicStorageModule.forRoot()],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: HttpsRequestInterceptor, multi: true }
    ]
})
export class HeaderInterceptorModule { }

// if the backend used sql you convert the response, use this instead:
/*
return fromPromise(this.storage.get('token')).pipe(switchMap(token => {
            const headers = new HttpHeaders({
                Authorization: `Bearer ${token}`
            })
            const dupReq = req.clone({ headers });
            return next.handle(dupReq).map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    if (event.body.data) {       // If event body response has data returned
                        return event.clone({
                            body: {...event.body, data: this.objConverter.convertResponse(event.body.data)} // Convert the response into a valid and well formatted object globally if response body data exist
                        })
                    }
                }
                return event;
              })
}))
*/
