import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { AlertMessageProvider } from '../../providers/alert-message/alert-message';

/**
 * Generated class for the BillingShippingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-billing-shipping',
  templateUrl: 'billing-shipping.html',
})
export class BillingShippingPage {
  cities: any = [];
  provinces: any = [];
  userData: any;
  loading: any;

  searchProvince: any;


  billingForm = new FormGroup({
    first_name: new FormControl(),
    last_name: new FormControl(),
    address_1: new FormControl(),
    country: new FormControl(),
    state: new FormControl(),
    city: new FormControl(),
    baranggay: new FormControl(),
    phone: new FormControl(),
    email: new FormControl(),
  })

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private api: ApiProvider,
    private storage: Storage,
    private viewCtrl: ViewController,
    private loadingCtrl: LoadingController,
    private alertMessage: AlertMessageProvider
  ) {


  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad BillingShippingPage');

    await this.getSpecificCustomerInfo()
    await this.formPopulate()
    await this.getProvinces()


  }

  async getProvinces() {
    const res: any = await this.api.getAllProvinces()
    this.provinces = res;
    console.log(this.provinces);
    console.log(this.billingForm.value.state);

    const selectedProvince = this.provinces.find(x => x.name === this.billingForm.value.state)
    console.log(selectedProvince);

    this.provinceSelect(selectedProvince)
  }


  async provinceSelect(selectedProvince) {

    if (selectedProvince) this.billingForm.value.state = selectedProvince.name
    console.log(this.billingForm.value);

    try {
      const cities: any = await this.api.getAllCities()
      const _cities = cities.filter(city => city.province === selectedProvince.key)
      this.cities = _cities
    } catch (err) {
      console.log(err)
    }
  }

  async getSpecificCustomerInfo() {
    const info = await this.storage.get('user_id')
    try {
      this.loadingController()
      const res: any = await this.api.getSpecificCustomer(info.id)
      res.meta_data.map(data => {
        if (data.key === 'billing_wooccm11') res.baranggay = data.value
      })
      this.userData = res
      this.loading.dismiss()
      console.log(res);
    } catch (error) {
      this.loading.dismiss()
      console.log(error);
    }
  }

  formPopulate() {

    this.billingForm = this.formBuilder.group({
      first_name: [
        this.userData.billing.first_name,
        Validators.compose([
          // Validators.pattern(this.globals.EMAIL_REGEX),
          Validators.required
        ])
      ],
      last_name: [
        this.userData.billing.last_name,
        Validators.compose([
          // Validators.pattern(this.globals.EMAIL_REGEX),
          Validators.required
        ])
      ],
      address_1: [
        this.userData.billing.address_1,
        Validators.compose([
          // Validators.pattern(this.globals.EMAIL_REGEX),
          Validators.required
        ])
      ],
      country: [
        'PH',
        Validators.compose([
          // Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]*"),
          Validators.required
        ])
      ],
      state: [
        this.userData.billing.state,
        Validators.compose([
          // Validators.pattern(this.globals.EMAIL_REGEX),
          Validators.required
        ])
      ],
      city: [
        this.userData.billing.city,
        Validators.compose([
          // Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]*"),
          Validators.required
        ])
      ],
      baranggay: [
        this.userData.baranggay,
        Validators.compose([
          // Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]*"),
          Validators.required
        ])
      ],
      phone: [
        this.userData.billing.phone,
        Validators.compose([
          // Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]*"),
          Validators.required
        ])
      ],
      email: [
        this.userData.email === '' ? this.navParams.get('email') : this.userData.email,
        Validators.compose([
          // Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]*"),
          Validators.required
        ])
      ],
    });


  }

  async changeCity(city) {
    this.billingForm.value.city = city.name
    console.log(this.billingForm.value);
    console.log(city);

  }

  async register() {
    const data = {
      first_name: this.billingForm.value.first_name,
      last_name: this.billingForm.value.last_name,
      email: this.billingForm.value.email,
      billing: this.billingForm.value,
      shipping: this.billingForm.value,
      meta_data: [
        {
          id: 401,
          key: "billing_wooccm11",
          value: this.billingForm.value.baranggay
        },
        {
          id: 403,
          key: "shipping_wooccm9",
          value: this.billingForm.value.baranggay
        }
      ]
    }

    console.log(data);

    try {
      this.loadingController()
      const res: any = await this.api.updateUser(this.userData.id, data)
      this.loading.dismiss()
      this.viewCtrl.dismiss(data)
      console.log(res);
    } catch (error) {
      this.loading.dismiss()
      console.log(error);
      this.alertMessage.open('Error Occurred', error.message, ['Try Again.'])
    }
  }


  dismiss() {
    this.viewCtrl.dismiss()
  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }



}
