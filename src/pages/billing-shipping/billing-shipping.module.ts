import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BillingShippingPage } from './billing-shipping';

@NgModule({
  declarations: [
    BillingShippingPage,
  ],
  imports: [
    IonicPageModule.forChild(BillingShippingPage),
  ],
})
export class BillingShippingPageModule {}
