import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the ContactUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {

  loading: any;
  contact: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private sanitize: DomSanitizer,
    private api: ApiProvider,
    private loadingCtrl: LoadingController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
    this.getContactUs()
  }

  async getContactUs() {
    try {
      this.loadingController()
      const res: any = await this.api.getSpecificPage('contact-us')
      this.contact = this.sanitize.bypassSecurityTrustHtml(res[0].content.rendered)
      this.loading.dismiss()
      console.log(this.contact);
    } catch (error) {
      this.loading.dismiss()
      this.presentAlertServerError()
      console.log(error);
      
    }

  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }


  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getContactUs()
        }
      },
      ]
    });
    alert.present();
  }


}
