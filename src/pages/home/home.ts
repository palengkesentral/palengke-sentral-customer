import { Component, ChangeDetectorRef, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, MenuController, Content, AlertController, Events, App } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  @ViewChild(Content) content: Content;
  backgroundImages: any;
  featuredProducts: any;
  loading: any;
  searchWord: any;
  bestSeller: any = []
  isLoadingBestSeller: boolean
  isLoadingFeature: boolean
  announcement: any;
  mediaAnnouncement: any;
  showAnnouncementMsg = true;
  staticBgImage = [
    // {
    //   media: 'assets/imgs/7.jpg'
    // },
    // {
    //   media: 'assets/imgs/8.jpg'
    // },
    {
      media: 'assets/imgs/6.jpg'
    },
    {
      media: 'assets/imgs/1.jpg'
    },
    {
      media: 'assets/imgs/2.jpg'
    },
    {
      media: 'assets/imgs/3.jpg'
    },
    {
      media: 'assets/imgs/4.jpg'
    },
    {
      media: 'assets/imgs/5.jpg'
    },

  ]

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private cf: ChangeDetectorRef,
    private loadingCtrl: LoadingController,
    private menuCtrl: MenuController,
    private alertCtrl: AlertController,
    private events: Events,
    private appCtrl: App
  ) {
    this.menuCtrl.enable(true, 'myMenu')

    this.events.subscribe('fromReceipt', () => {
      setTimeout(() => {
        this.appCtrl.getActiveNavs()[0].push('AccountPage')
      }, 500);
    })
  }

  ionViewDidLoad() {
    this.getAnnouncement()
    this.getProducts()
    this.getBestSeller()
  }

  ionViewWillLeave() {
    this.events.unsubscribe('fromReceipt')
  }


  ionViewDidEnter() {
    let elements = document.querySelectorAll(".tabbar");
    if (elements != null) {
      Object.keys(elements).map((key) => {
        elements[key].style.display = 'flex';
      });
    }

    setTimeout(() => { this.content.resize(); }, 500);

  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getProducts()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }


  goToProductDetails(product) {
    this.navCtrl.push('ProductDetailsPage', { product: product }, { animation: 'ios-transition' })
  }

  async getProducts() {
    try {
      this.isLoadingFeature = true
      const res: any = await this.api.getAllProducts(true)
      const productsTopSales = res.sort((a, b) => a.total_sales < b.total_sales).slice(0, 10)
      productsTopSales.map(product => {
        product.meta_data.map(data => {
          if (data.key === '_woo_uom_input') {
            product.unit = data
          }
        })
      })
      this.isLoadingFeature = false
      this.featuredProducts = productsTopSales;
      console.log(this.featuredProducts);
    } catch (error) {
      this.isLoadingFeature = false
      this.presentAlertServerError()
      console.log(error);
    }
  }

  async getBestSeller() {
    try {
      this.isLoadingBestSeller = true
      let arr = []
      const res: any = await this.api.getBestSeller()
      console.log('best seller', res);
      res.length = 5
      res.map(async data => {
        try {
          const product: any = await this.api.getSpecificProduct(data.product_id)
          console.log(product);
          arr.push(product)
        } catch (error) {
          console.log(error);
          this.isLoadingBestSeller = false
        }
      })

      setTimeout(() => {
        this.bestSeller = arr
        console.log(this.bestSeller);
        this.isLoadingBestSeller = false
      }, 5000);

    } catch (error) {
      console.log(error);
    }
  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }

  goToSearch() {
    this.navCtrl.push('SearchPage', {}, { animation: 'ios-transition' })
  }


  async getAnnouncement() {
    try {
      const res: any = await this.api.getAnnouncement()
      console.log(res);
      this.announcement = res[0]
      if (res.length !== 0) await this.getMediaAnnouncement(res[0].featured_media)
    } catch (error) {
      console.log(error);
    }
  }

  async getMediaAnnouncement(media_id) {
    try {
      const res: any = await this.api.getMediaAnnouncement(media_id)
      this.mediaAnnouncement = res.source_url
      console.log(this.mediaAnnouncement);
    } catch (error) {
      console.log(error);
    }
  }

  closeAnnouncement() {
    this.showAnnouncementMsg = false;
  }


  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getProducts()
          this.events.publish('refreshUserInfo')
        }
      },
      ]
    });
    alert.present();
  }

}

