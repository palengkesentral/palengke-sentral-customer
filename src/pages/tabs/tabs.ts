import { Component, ViewChild } from '@angular/core';

import { Tabs, IonicPage, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild('myTabs') tabRef: Tabs;


  tabsContent = [
    {
      root: 'HomePage',
      tabTitle: 'Home',
      icon: "home",
      badge: 0
    },
    {
      root: 'ProductCategoryPage',
      tabTitle: 'Shop',
      icon: "",
      badge: 0
    },
    {
      root: 'CartPage',
      tabTitle: 'Bayong',
      icon: "basket",
      badge: 0
    }
  ];

  tab1Root = 'HomePage';
  tab2Root = 'ProductCategoryPage';
  tab3Root = 'AccountPage';

  constructor(
    private events: Events,
    private storage: Storage,
  ) {
    this.events.subscribe('addedToCart', (cart) => {
      console.log('EVENT', cart);
      this.tabsContent[2].badge = cart.length
    });



  }

  // ionViewWillLeave() {
  //   this.events.unsubscribe('addedToCart')
  // }

  async ngOnInit() {
    const cart = await this.storage.get('cart')
    this.tabsContent[2].badge = cart.length
  }


  onTabChanged(ev) {
    const previousTab = this.tabRef.previousTab(false);

    if (previousTab) {
      try {
        // Get the navCtrl and pop to the root page
        previousTab.getViews()[0].getNav().popToRoot();
      } catch (exception) {
        // Oops...
        console.error(exception);
      }
    }
  }
}
