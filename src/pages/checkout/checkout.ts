import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, Nav, Events, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { AlertMessageProvider } from '../../providers/alert-message/alert-message';

/**
 * Generated class for the CheckoutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})
export class CheckoutPage {
  @ViewChild('myInput') myInput: ElementRef;
  resize() {
    let textArea = this.myInput['_elementRef'].nativeElement.getElementsByTagName('textarea')[0];
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + "px";
  }


  enabledPaymentGateways: any = []
  userData: any;
  shipping_line: any;
  total: any;
  selectedPayment: boolean;
  selectedGateway: any;
  notes: any = "";
  loading: any;
  couponLine: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private storage: Storage,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private nav: Nav,
    private events: Events,
    private alertCtrl: AlertController,
    private alertMessage: AlertMessageProvider
  ) {

    this.shipping_line = this.navParams.get('shipping')
    this.total = this.navParams.get('total')
    this.couponLine = this.navParams.get('coupon_line')
    console.log(this.couponLine);

  }

  async ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentGatewayPage');
    this.getSpecificCustomerInfo()
    this.getPaymentGateways()

    if (this.couponLine) {
      this.couponLine = [{
        code: this.couponLine.code,
        discount: this.couponLine.amount,
        discount_tax: "0",
        meta_data: [{
          value: this.couponLine
        }]
      }]
    } else if (this.couponLine === undefined) {
      this.couponLine = []
    }

  }

  async getPaymentGateways() {
    try {
      const res: any = await this.api.getPaymentGateway()
      console.log(res);
      
      res.map(data => {
        data.selected = false
      })
      this.enabledPaymentGateways = res.filter(data => {
        if (data.enabled === true) return data
      })
      // this.enabledPaymentGateways = res
      console.log(this.enabledPaymentGateways);
    } catch (error) {
      console.log(error);
    }

  }

  checkSelected(gateway) {
    this.selectedGateway = gateway
    this.selectedPayment = gateway.selected
    for (let i = 0; i < this.enabledPaymentGateways.length; i++) {
      if (this.enabledPaymentGateways[i].id != gateway.id) this.enabledPaymentGateways[i].selected = false;
    }
  }

  async getSpecificCustomerInfo() {
    const info = await this.storage.get('user_id')
    try {
      this.loadingController()
      const res: any = await this.api.getSpecificCustomer(info.id)
      this.loading.dismiss()
      this.userData = res
      console.log(res);
    } catch (error) {
      this.loading.dismiss()
      this.presentAlertServerError()
      console.log(error);
    }
  }

  presentEditProfile() {
    let profileModal = this.modalCtrl.create('BillingShippingPage');
    profileModal.onDidDismiss((data) => {
      if (data) this.getSpecificCustomerInfo()
    });
    profileModal.present();
  }


  doubleCheckOrder() {

    let alert = this.alertCtrl.create({
      title: 'Are you sure?',
      subTitle: 'Confirm placing an order to us.',
      buttons: [
        {
          text: 'Cancel',
          handler: () => { },
        },
        {
          text: 'Proceed',
          handler: () => {
            this.placeOrder()
          },
        },
      ]
    });
    alert.present();


  }

  async placeOrder() {
    let items = []

    const products = await this.storage.get('cart')
    products.map(data => {
      const line = {
        product_id: data.id,
        quantity: data.number_product
      }
      items.push(line)
    })

    console.log(items);


    let data = {
      payment_method: this.selectedGateway.id,
      payment_method_title: this.selectedGateway.method_title,
      billing: this.userData.billing,
      shipping: this.userData.shipping,
      line_items: items,
      customer_note: this.notes,
      customer_id: this.userData.id,
      status: this.selectedGateway.id === 'cod' ? 'processing' : 'pending',
      shipping_lines: [{
        method_id: this.shipping_line.method_id,
        method_title: this.shipping_line.method_title,
        total: this.shipping_line.method_id === 'free_shipping' ? '0' : this.shipping_line.settings.cost.value
      }],
      coupon_lines: this.couponLine
    }

    console.log(data);

    try {
      this.loadingController()
      const res: any = await this.api.createOrder(data)
      console.log(res);
      await this.storage.set('cart', [])
      await this.storage.set('totalAmount', 0)
      await this.storage.remove('useCoupon')
      this.events.publish('addedToCart', [])
      this.loading.dismiss()
      this.nav.setRoot('ReceiptPage', { orderDetails: res })
    } catch (error) {
      this.loading.dismiss()
      this.alertMessage.open('Error Occurred', error.error.message, ['OK'])
      console.log(error);
    }
  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getSpecificCustomerInfo()
    this.getPaymentGateways()
    this.selectedPayment = false
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getSpecificCustomerInfo()
          this.getPaymentGateways()
          this.selectedPayment = false
        }
      },
      ]
    });
    alert.present();
  }

}
