import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App, Events, Nav } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the ReceiptPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-receipt',
  templateUrl: 'receipt.html',
})
export class ReceiptPage {
  
  orderDetails: any;
  deliveryDate: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    private app: App,
    private events: Events,
    private nav: Nav
    ) {

      this.orderDetails = this.navParams.get('orderDetails')
      this.getTheDeliveryDate()
      console.log(this.orderDetails);
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiptPage');
    
  }

  async backToHome() {
    this.events.publish('fromReceipt')
    await this.nav.setRoot('TabsPage')
  }


  getTheDeliveryDate() {
    const dateOrdered = new Date(this.orderDetails.date_created).getTime()
    const checkDeliveryDate = new Date(dateOrdered).toDateString()
    this.deliveryDate = new Date(dateOrdered + (checkDeliveryDate.slice(0,3) === 'Sat' ? 48 : 24) * 60 * 60 * 1000).toDateString()
  }

}
