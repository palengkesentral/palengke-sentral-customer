import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, Events, MenuController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';

import { Storage } from '@ionic/storage';
import { AlertMessageProvider } from '../../providers/alert-message/alert-message';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  loginForm: FormGroup;
  isShowPassword: boolean = false;
  passwordType: string = 'password';
  loading: any;
  fbInfo: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private api: ApiProvider,
    private storage: Storage,
    private alert: AlertMessageProvider,
    private iab: InAppBrowser,
    private fb: Facebook,
    private googlePlus: GooglePlus,
    private loadingCtrl: LoadingController,
    private event: Events,
    private menuCtrl: MenuController,
    private alertCtrl: AlertController
  ) {
    this.menuCtrl.enable(false, 'myMenu')

    this.loginForm = formBuilder.group({
      username: [
        '',
        Validators.compose([
          // Validators.pattern(this.globals.EMAIL_REGEX),
          Validators.required
        ])
      ],
      password: [
        '',
        Validators.compose([
          // Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]*"),
          Validators.required
        ])
      ]
    });
  }

  ionViewDidLoad() {
    let elements = document.querySelectorAll(".tabbar");
    if (elements != null) {
      Object.keys(elements).map((key) => {
        elements[key].style.display = 'none';
      });
    }
  }


  ngOnDestroy(): void {
    let elements = document.querySelectorAll(".tabbar");
    if (elements != null) {
      Object.keys(elements).map((key) => {
        elements[key].style.display = 'flex';
      });
    }
  }

  showPassword() {
    this.isShowPassword = !this.isShowPassword
    if (this.isShowPassword) this.passwordType = 'text'
    else this.passwordType = 'password'
  }


  async login() {
    try {
      this.loadingController()
      const res: any = await this.api.loginUser(this.loginForm.value)
      console.log(res);
      this.storage.set('loginType', 'normal')
      await this.storage.set('token', res.token)
      await this.navCtrl.setRoot('TabsPage')
      this.event.publish('isLoggedIn')
      this.loading.dismiss()
    } catch (error) {
      this.alert.open('Error Occurred!', error.error.message === undefined ? 'Server error. Please try again or please check your internet connection' : error.error.message, ['OK'])
      this.loading.dismiss()
      console.log(error);
    }
  }

  async loginFacebook() {
    try {
      const permission = ['public_profile', 'email']
      const res: FacebookLoginResponse = await this.fb.login(permission)
      console.log(res);
      const fbInfo: any = await this.fb.api('/me?fields=name,email', permission)
      fbInfo.picture = "https://graph.facebook.com/" + res.authResponse.userID + "/picture?type=large";
      this.fbInfo = fbInfo
      this.storage.set('fbInfo', fbInfo)
      this.storage.set('loginType', 'fb')
      if (res.status === 'connected') {
        this.loadingController()
        const fbTokenInfo: any = await this.api.facebookLogin(res.authResponse.accessToken)
        console.log(fbTokenInfo);
        await this.storage.set('token', fbTokenInfo.token)
        await this.navCtrl.setRoot('TabsPage')
        this.event.publish('isFbLoggedIn', this.fbInfo)
        this.event.publish('isLoggedIn')
        this.loading.dismiss()
      }
    } catch (error) {
      console.log(error);
      if (error.status === 0) {
        this.loading.dismiss()
        this.presentAlertFacebookFirstLogin()
      } else {
        this.loading.dismiss()
        this.alert.open('Error Occurred!', error.errorMessage === undefined ? 'Server error. Please try again or please check your internet connection' : error.error.message, ['OK'])
      }

    }
  }


  async loginGmail() {
    try {
      const res: any = await this.googlePlus.login({
        'webClientId': '827651102930-j0urq6g39o3v5ct6621dca0698h4mot4.apps.googleusercontent.com',
        'offline': true
      })
      console.log(res);
      this.storage.set('loginType', 'google')
    } catch (error) {
      console.log(error);
    }
  }

  goToRegister() {
    this.navCtrl.push('RegisterPage', {}, { animation: 'ios-transition' })
  }

  goToForgotPassword() {
    this.iab.create('https://palengkesentral.com/my-account/lost-password/')
  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }

  presentAlertFacebookFirstLogin() {
    let alert = this.alertCtrl.create({
      title: 'Facebook Login',
      subTitle:  ` <div class="custom-spinner-container">
                    <img src="${this.fbInfo.picture}" width="100px" height="100px" class="br-50">
                    </div>
                    <p class="text-center font-18 montserrat-bold">Hello! ${this.fbInfo.name} with an email of ${this.fbInfo.email}</p>`,
      buttons: [{
        text: 'Login with Facebook',
        handler: () => {
          this.loginFacebook()
          this.event.publish('isFbLoggedIn', this.fbInfo)
          this.event.publish('isLoggedIn')
        }
      },
      ]
    });
    alert.present();
  }


}
