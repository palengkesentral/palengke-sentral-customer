import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';
import { AlertMessageProvider } from '../../providers/alert-message/alert-message';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  registerForm: FormGroup
  isShowPassword: boolean = false;
  passwordType: string = 'password';
  loading: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private api: ApiProvider,
    private alert: AlertMessageProvider,
    private loadingCtrl: LoadingController
  ) {

    this.registerForm = formBuilder.group({
      email: [
        '',
        Validators.compose([
          // Validators.pattern(this.globals.EMAIL_REGEX),
          Validators.required
        ])
      ],
      username: [
        '',
        Validators.compose([
          // Validators.pattern(this.globals.EMAIL_REGEX),
          Validators.required
        ])
      ],
      password: [
        '',
        Validators.compose([
          // Validators.pattern("[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]*"),
          Validators.required
        ])
      ]
    });

  }

  // ionViewDidLoad() {
  //   let elements = document.querySelectorAll(".tabbar");
  //   if (elements != null) {
  //     Object.keys(elements).map((key) => {
  //       elements[key].style.display = 'none';
  //     });
  //   }
  // }


  // ngOnDestroy(): void {
  //   let elements = document.querySelectorAll(".tabbar");
  //   if (elements != null) {
  //     Object.keys(elements).map((key) => {
  //       elements[key].style.display = 'flex';
  //     });
  //   }
  // }

  async register() {
    try {
      this.loadingController()
      const res: any = await this.api.registerUser(this.registerForm.value)
      console.log(res);
      this.loading.dismiss()
      this.alert.open('Success!', 'Thank you for creating an account in Palengke Sentral.', ['OK'])
      this.navCtrl.setRoot('LoginPage')
    } catch (error) {
      this.alert.open('Error Occurred!', error.error.message === undefined ? 'Server error. Please try again or please check your internet connection' : error.error.message, ['OK'])
      this.loading.dismiss()
      console.log(error);
    }


    // this.navCtrl.setRoot('LoginPage')
  }

  showPassword() {
    this.isShowPassword = !this.isShowPassword
    if (this.isShowPassword) this.passwordType = 'text'
    else this.passwordType = 'password'
  }


  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }

}
