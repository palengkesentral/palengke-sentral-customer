import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  @ViewChild(Content) content: Content;
  searchWord: any;
  words: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

  ionViewDidEnter() {
    let elements = document.querySelectorAll(".tabbar");
      if (elements != null) {
        Object.keys(elements).map((key) => {
          elements[key].style.display = 'flex';
        });
      }

      setTimeout(() => { this.content.resize(); }, 500);
  }

  async onInput() {
    console.log(this.searchWord);
    try {
      const res: any = await this.api.searchProduct(this.searchWord)
      this.words = res
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  }

  goToProductDetails(product) {
    this.navCtrl.push('ProductDetailsPage', { product: product }, { animation: 'ios-transition' })
  }


}
