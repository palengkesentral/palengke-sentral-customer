import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, App, Content, LoadingController, AlertController, Slides } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { GlobalsProvider } from '../../providers/globals/globals';
import { ToastMessagesProvider } from '../../providers/toast-messages/toast-messages';
import { Toast } from '@ionic-native/toast';
import { AnonymousSubject } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';



/**
 * Generated class for the CartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cart',
  templateUrl: 'cart.html',
})
export class CartPage {
  @ViewChild(Content) content: Content;
  @ViewChild(Slides) slides: Slides
  cartProducts: any = [];
  subTotal: number = 0;
  shippingFee: number = 0;
  totalPayment: number = 0;
  loading: any;
  shippingStatus: any;
  minimumShipping: any;
  coupons: any = [];
  couponUsed: any;
  successCoupon: any;
  isValidCoupon: boolean = false;
  couponMessage: any = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private storage: Storage,
    private events: Events,
    private app: App,
    private global: GlobalsProvider,
    private toast: Toast,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
  }

  async ionViewDidEnter() {
    if (this.minimumShipping === undefined) {
      await this.getMinimumShipping()
      await this.getShippingFeeStatus()
      await this.getCoupons()
    }

    await this.getCartProducts();
    // if (await this.storage.get('useCoupon')) {
    //   this.successCoupon = await this.storage.get('useCoupon')
    //   this.couponUsed = this.successCoupon.code
    // } else {
    //   this.successCoupon = undefined
    // }


    let elements = document.querySelectorAll(".tabbar");
    if (elements != null) {
      Object.keys(elements).map((key) => {
        elements[key].style.display = 'flex';
      });
    }

    setTimeout(() => { this.content.resize(); }, 500);
  }

  async ionViewDidLeave() {
    // if (await this.storage.get('useCoupon')) await this.storage.remove('useCoupon')
    this.couponUsed = ''
    this.successCoupon = undefined
    this.couponMessage = ''
    this.isValidCoupon = false
  }


  goToProductDetails(product) {
    this.navCtrl.push('ProductDetailsPage', { product: product }, { animation: 'ios-transition' })
  }

  toggleNumber(product, operation) {
    if (operation === 'add') {
      product.number_product++
      this.changeNumberOfOrder()
    } else if (operation === 'subtract') {
      product.number_product--
      this.changeNumberOfOrder()
    }
  }

  async getShippingFeeStatus() {
    try {
      const res: any = await this.api.getShippingFee(2, 2) // place and method
      this.shippingStatus = res
      console.log(this.shippingStatus);
    } catch (error) {
      console.log(error);
    }
  }

  async getMinimumShipping() {
    try {
      this.loadingController()
      const res: any = await this.api.getShippingFee(2, 3) // place and method
      this.minimumShipping = res
      this.loading.dismiss()
      console.log(this.minimumShipping);
    } catch (error) {
      this.loading.dismiss()
      this.presentAlertServerError()
      console.log(error);
    }
  }


  async getCartProducts() {
    this.cartProducts = await this.storage.get('cart')
    console.log(this.cartProducts);
    this.getSubTotal(this.cartProducts)
  }

  async removeProduct(product) {
    const removeIndex = this.global.cart.map(item => { return item.id; }).indexOf(product.id);
    this.global.cart.splice(removeIndex, 1)
    this.storage.set('cart', this.global.cart)
    this.cartProducts = this.global.cart
    this.subtractTotalAmount(product)
    this.slides.slideTo(0, 1000)
    this.presentToast(`${product.name} is removed in the cart`)
    this.events.publish('addedToCart', this.global.cart)
    console.log(this.global.cart);
    await this.getSubTotal(this.global.cart)
  }

  async subtractTotalAmount(product) {
    const amt = await this.storage.get('totalAmount')
    this.global.totalAmount = amt - (parseInt(product.price) * product.number_product)
    this.storage.set('totalAmount', this.global.totalAmount)
  }

  async changeNumberOfOrder() {
    this.storage.set('cart', this.cartProducts)
    await this.getSubTotal(this.cartProducts)
  }

  async getSubTotal(cartProducts) {
    let sum = []

    for (let i = 0; i < cartProducts.length; i++) {
      const pricePerProduct = parseInt(cartProducts[i].price) * cartProducts[i].number_product
      sum.push(pricePerProduct)
    }

    this.subTotal = sum.reduce((acc, val) => {
      return acc + val
    }, 0)

    console.log(this.subTotal);
    this.global.totalAmount = this.subTotal
    this.storage.set('totalAmount', this.subTotal)

    // if (this.subTotal > 1500) this.shippingFee = 0
    // else this.shippingFee = 50

    if (this.subTotal > parseInt(this.minimumShipping.settings.min_amount.value)) this.shippingFee = 0
    else this.shippingFee = parseInt(this.shippingStatus.settings.cost.value)

    this.totalPayment = (this.subTotal + this.shippingFee) - (this.successCoupon === undefined ? 0 : this.successCoupon.amount)



  }

  checkout() {
    this.navCtrl.push('CheckoutPage', { shipping: this.shippingFee === 0 ? this.minimumShipping : this.shippingStatus, total: this.totalPayment, coupon_line: this.successCoupon}, { animation: 'ios-transition' })
  }

  goToShop() {
    this.navCtrl.setRoot('ProductCategoryPage')
  }

  async getCoupons() {
    try {
      const res: any = await this.api.getCoupons()
      this.coupons = res;
      console.log(res);
    } catch (error) {
      console.log(error);
    }
  }

  validateCoupon() {
    console.log(this.couponUsed);
    this.coupons.filter(async data => {
      if (data.code === this.couponUsed.toLowerCase()) {
        console.log(data);
        if (parseInt(data.minimum_amount) < this.subTotal) {
          if (Date.now() > (new Date(data.date_expires)).getTime()) {
            this.couponMessage = `Coupon (${this.couponUsed}) is expired.`
            this.isValidCoupon = false
            this.successCoupon = undefined
          } else {
            console.log('di pa expired g lang');
            this.isValidCoupon = true
            // this.storage.set('useCoupon', data)
            this.successCoupon = data
            this.totalPayment = this.totalPayment - parseInt(data.amount)
            this.couponMessage = `Applied Coupon (${data.code}).`
          }
        } else {
          console.log('not pasok sa presyo');
          this.isValidCoupon = false
          this.couponMessage = `Coupon (${this.couponUsed}) is applicable only in the minimum order of ${data.minimum_amount}.`
          this.successCoupon = undefined
        }
      } else {
        console.log('NO COUPON AVAILABLE');
        this.isValidCoupon = false
        this.couponMessage = `Coupon (${this.couponUsed}) is invalid.`
        this.successCoupon = undefined
      }
    })
  }

  presentToast(msg) {
    this.toast.showWithOptions({
      message: msg,
      duration: 3000,
      position: "bottom",
      styling: {
        backgroundColor: '#1E1E1F',
        textColor: '#FCFCFF',
      }
    }).subscribe(toast => { console.log(toast); });
  }


  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }


  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getMinimumShipping()
          this.getShippingFeeStatus()
        }
      },
      ]
    });
    alert.present();
  }


}
