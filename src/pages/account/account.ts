import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiProvider } from '../../providers/api/api';
import { Storage } from '@ionic/storage';
import { timestamp } from 'rxjs/operators';
import { ThrowStmt } from '@angular/compiler';

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  orders: any;
  segment: any;
  userData: any;
  loading: any;
  loginType: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    private api: ApiProvider,
    private storage: Storage,
    private modalCtrl: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {

    this.segment = 'processing'
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
    this.getSpecificCustomerInfo()
    this.getOrderByCustomer()
  }

  async getOrderByCustomer() {
    const customerId: any = await this.storage.get('user_id')
    try {
      this.loadingController()
      const res: any = await this.api.getOrderByCustomer(customerId.id, this.segment)
      this.loading.dismiss()
      this.orders = res
      console.log(res);
    } catch (error) {
      this.loading.dismiss()
      this.presentAlertServerError()
      console.log(error);
    }
  }

  async segmentChanged(ev) {
    console.log(ev.value);
    this.segment = await ev.value
    await this.getOrderByCustomer()
  }

  async getSpecificCustomerInfo() {
    this.loginType = await this.storage.get('loginType')
    if (this.loginType === 'fb') {
      const info = await this.storage.get('fb_info')
      this.userData = info
      console.log('fb', this.userData);
    } else {
      const info = await this.storage.get('user_id')
      try {
        const res: any = await this.api.getSpecificCustomer(info.id)
        this.userData = res
        console.log('normal', this.userData);
      } catch (error) {
      }

    }

  }

  openEditProfileModal() {
    let profileModal = this.modalCtrl.create('BillingShippingPage', { email: this.userData.email });
    profileModal.onDidDismiss((data) => {
      if (data) this.getSpecificCustomerInfo()
    });
    profileModal.present();
  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    // this.getSpecificCustomerInfo()
    this.getOrderByCustomer()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getOrderByCustomer()
        }
      },
      ]
    });
    alert.present();
  }

}
