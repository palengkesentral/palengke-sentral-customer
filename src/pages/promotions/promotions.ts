import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { DomSanitizer } from '@angular/platform-browser';

/**
 * Generated class for the PromotionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-promotions',
  templateUrl: 'promotions.html',
})
export class PromotionsPage {

  promos: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private api: ApiProvider,
    private sanitize: DomSanitizer
    ) {
  }

  ionViewDidLoad() {
    this.getPromotions();
  }

  async getPromotions() {
    const res: any = await this.api.getSpecificPage('promos')
    this.promos = this.sanitize.bypassSecurityTrustHtml(res[0].content.rendered)
    console.log(this.promos);
  }

}
