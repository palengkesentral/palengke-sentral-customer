import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the WalkthroughPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {
  @ViewChild(Slides) slides: Slides
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalkthroughPage');
  }

  loginPage() {
    this.navCtrl.setRoot('LoginPage').then(() => {
      this.storage.set('introShown', true)
    })
  }


  next() {
    if (!this.slides.isEnd()) this.slides.slideNext()
  }


  skip() {
    this.slides.slideTo(3)
  }

}
