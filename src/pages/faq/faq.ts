import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';

import { DomSanitizer } from '@angular/platform-browser';
/**
 * Generated class for the FaqPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {

  faqs: any;
  loading: any;

  faqList = [
    {
      title: 'Test Title',
      description: 'Lorem Ipsum!',
      isShow: false
    },
    {
      title: 'Test Title',
      description: 'Lorem Ipsum!',
      isShow: false
    },
    {
      title: 'Test Title',
      description: 'Lorem Ipsum!',
      isShow: false
    },
    {
      title: 'Test Title',
      description: 'Lorem Ipsum!',
      isShow: false
    },
  ]
  isShow: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private sanitize: DomSanitizer,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FaqPage');
    this.getFaqs()
  }

  handleFaqClick(faq) {
    faq.isShow = !faq.isShow;
  }

  async getFaqs() {
    try {
      this.loadingController()
      const res: any = await this.api.getSpecificPage('faqs')
      this.loading.dismiss()
      this.faqs = this.sanitize.bypassSecurityTrustHtml(res[0].content.rendered)
      console.log(this.faqs);
    } catch (error) {
      this.loading.dismiss()
      this.presentAlertServerError()
    }

  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getFaqs()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getFaqs()
        }
      },
      ]
    });
    alert.present();
  }

}
