import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController, ViewController, Content, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';
import { Storage } from '@ionic/storage';
import { ToastMessagesProvider } from '../../../providers/toast-messages/toast-messages';
import { GlobalsProvider } from '../../../providers/globals/globals';
import { Toast } from '@ionic-native/toast';
import { AlertMessageProvider } from '../../../providers/alert-message/alert-message';

/**
 * Generated class for the ProductDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-details',
  templateUrl: 'product-details.html',
})
export class ProductDetailsPage {
  @ViewChild('bottomSheet', { read: ElementRef }) bottomSheet
  @ViewChild(Content) content: Content;
  @HostListener("window:scroll", ["$event"])

  segment: any;
  productDetail: any;
  relatedProducts: any = [];
  loading: any;
  priceView: any;
  cartProduct: any = [];
  displayAmount: any;
  isLoading: boolean

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private storage: Storage,
    private events: Events,
    private global: GlobalsProvider,
    private loadingCtrl: LoadingController,
    private viewCtrl: ViewController,
    private toast: Toast,
    private alertCtrl: AlertController,
    private alertMessage: AlertMessageProvider
  ) {

    this.segment = 'description'
    this.productDetail = this.navParams.get('product')
    this.productDetail.number_product = 1
    console.log(this.productDetail);

  }

  ionViewDidEnter() {
    this.getGlobalAmount()
    this.getCart()
  }

  ngOnInit() {
    let elements = document.querySelectorAll(".tabbar");
    if (elements != null) {
      Object.keys(elements).map((key) => {
        elements[key].style.display = 'none';
      });
    }

    this.getRelatedProducts()

  }


  async getCart() {
    this.global.cart = await this.storage.get('cart')
  }

  slideChanged() {
    setTimeout(() => {
      this.content.resize();
    }, 1000);



  }




  ngOnDestroy() {
    if (this.viewCtrl.name !== 'ProductDetailsPage') {
      let elements = document.querySelectorAll(".tabbar");
      if (elements != null) {
        Object.keys(elements).map((key) => {
          elements[key].style.display = 'flex';
        });
      }
    }
  }

  segmentChanged(ev) {
    console.log(ev.value);
    this.segment = ev.value
  }

  goToProductDetails(product) {
    this.navCtrl.push('ProductDetailsPage', { product: product }, { animation: 'ios-transition' })
  }

  async getRelatedProducts() {
    try {
      this.isLoading = true
      const res: any = await this.api.getAllRelatedProducts(this.productDetail.related_ids)
      res.map(product => {
        product.meta_data.map(data => {
          if (data.key === '_woo_uom_input') {
            product.unit = data
          }
        })
      })
      this.isLoading = false
      this.relatedProducts = res
      console.log(res);
    } catch (error) {
      this.isLoading = false
      this.presentAlertServerError()
      console.log(error);
    }
  }

  async addToCart(product) {
    try {
      // const res: any = await this.storage.get('cart')

      var index = this.global.cart.findIndex(x => x.id === product.id)

      if (index === -1) {
        this.global.cart.push(product)
        this.storage.set('cart', this.global.cart)
        this.computeTotalAmount(product)
        this.events.publish('addedToCart', this.global.cart)
        this.presentToast(`${product.name} is added in the cart`)
      } else {
        this.presentToast('Already added in the cart')
        console.log('already in cart');
      }

      this.navCtrl.pop()
      console.log(this.global.cart);
    } catch (error) {
      console.log(error);
      this.alertMessage.open('Error Occurred', 'Please try again.')
    }

  }


  toggleNumber(product, operation) {
    if (operation === 'add') {
      product.number_product++
      this.priceView = product.number_product * parseInt(product.price)
      this.changeNumberOfOrder(product)
    } else if (operation === 'subtract') {
      product.number_product--
      this.priceView = product.number_product * parseInt(product.price)
      this.changeNumberOfOrder(product)
    }
  }


  async changeNumberOfOrder(product) {

    // const res: any = await this.storage.get('cart')
    if (this.global.cart) {
      for (let i = 0; i < this.global.cart.length; i++) {
        if (this.global.cart.id === product.id) {
          this.global.cart.push(product)
          this.storage.set('cart', this.global.cart)
          this.events.publish('addedToCart', this.global.cart)
        }
      }
    } else {
      this.global.cart.push(product)
      this.storage.set('cart', this.global.cart)
      this.events.publish('addedToCart', this.global.cart)
    }
    console.log(this.global.cart);
    this.storage.set('cart', this.global.cart)
  }

  async getGlobalAmount() {
    const amt = await this.storage.get('totalAmount')
    this.displayAmount = amt
  }

  async computeTotalAmount(product) {
    const amt = await this.storage.get('totalAmount')
    this.global.totalAmount = (parseInt(product.price) * product.number_product) + amt
    this.displayAmount = this.global.totalAmount
    this.storage.set('totalAmount', this.global.totalAmount)
  }


  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }

  presentToast(msg) {
    this.toast.showWithOptions({
      message: msg,
      duration: 3000,
      position: "bottom",
      styling: {
        backgroundColor: '#1E1E1F',
        textColor: '#FCFCFF',
      }
    }).subscribe(toast => { console.log(toast); });
  }


  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getRelatedProducts()
        }
      },
      ]
    });
    alert.present();
  }

}
