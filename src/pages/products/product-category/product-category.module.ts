import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductCategoryPage } from './product-category';
import { ComponentsModule } from '../../../components/components.module';

@NgModule({
  declarations: [
    ProductCategoryPage,
  ],
  imports: [
    IonicPageModule.forChild(ProductCategoryPage),
    ComponentsModule
  ],
})
export class ProductCategoryPageModule {}
