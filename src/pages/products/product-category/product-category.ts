import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';

/**
 * Generated class for the ProductCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product-category',
  templateUrl: 'product-category.html',
})
export class ProductCategoryPage {

  categories: any = [];
  loading: any;
  isLoading: boolean;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {
    this.getCategories() 
    console.log('ionViewDidLoad ProductCategoryPage');
  }


  openPage(page, category) {
    this.navCtrl.push(page, { categoryId: category.id, categoryName: category.name }, { animation: 'ios-transition' })
  }


  async getCategories() {
    try {
      this.isLoading = true
      const res: any = await this.api.getAllCategories()
      const dataFiltered = res.filter(data => {
        if (data.parent === 0) return data;
      })
      this.isLoading = false
      this.categories = dataFiltered;
      console.log(this.categories);
    } catch (error) {
      this.loading.dismiss()
      this.presentAlertServerError() 
      console.log(error);
    }
  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }


  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.getCategories() 
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getCategories() 
        }
      },
      ]
    });
    alert.present();
  }

}
