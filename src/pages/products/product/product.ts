import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, LoadingController, Content, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../../providers/api/api';
import { Storage } from '@ionic/storage';
import { GlobalsProvider } from '../../../providers/globals/globals';
import { Toast } from '@ionic-native/toast';
import { AlertMessageProvider } from '../../../providers/alert-message/alert-message';
/**
 * Generated class for the ProductPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})
export class ProductPage {
  @ViewChild(Content) content: Content;
  categoryId: any;
  categoryName: any;
  products: any = [];
  numberProducts: number = 1;
  count: number = 1;
  cart: any = [];
  loading: any;
  searchWord: any = '';
  displayAmount: any;
  countClicked: number = 0
  todayDate: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private api: ApiProvider,
    private storage: Storage,
    private events: Events,
    private global: GlobalsProvider,
    private toast: Toast,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private alertMessage: AlertMessageProvider
  ) {

    this.categoryId = this.navParams.get('categoryId')
    this.categoryName = this.navParams.get('categoryName')
    console.log(this.categoryName);

    this.todayDate = new Date().toDateString()

  }

  async ionViewDidLoad() {
    await this.getCart()
    await this.getProducts()
    await this.getGlobalAmount()

  }

  ionViewDidEnter() {
    let elements = document.querySelectorAll(".tabbar");
    if (elements != null) {
      Object.keys(elements).map((key) => {
        elements[key].style.display = 'flex';
      });
    }

    setTimeout(() => { this.content.resize(); }, 500);
  }

  async getCart() {
    this.global.cart = await this.storage.get('cart')
  }

  goToProductDetails(product) {
    this.navCtrl.push('ProductDetailsPage', { product: product }, { animation: 'ios-transition' })
  }

  toggleNumber(product, operation) {
    if (operation === 'add') {
      product.number_product++
      this.changeNumberOfOrder(product)
    } else if (operation === 'subtract') {
      product.number_product--
      this.changeNumberOfOrder(product)
    }
  }

  trackByFn(id) {
    return id
  }

  async getProducts() {
    try {
      this.loadingController()
      const res: any = await this.api.getProductsPerCategory(this.categoryId, this.count)
      res.map(data => {
        data.number_product = 1
        data.date_created = new Date(data.date_created).toDateString()
      })
      res.map(product => {
        product.meta_data.map(data => {
          if (data.key === '_woo_uom_input') {
            product.unit = data
          }
        })
      })


      this.loading.dismiss()
      this.products = res;
      console.log(this.products);
    } catch (error) {
      this.loading.dismiss()
      this.presentAlertServerError()
      console.log(error);
    }
  }

  async addToCart(product) {
    try {
      // const res: any = await this.storage.get('cart')
      // console.log(res);

      var index = this.global.cart.findIndex(x => x.id === product.id)

      if (index === -1) {
        this.global.cart.push(product)
        this.storage.set('cart', this.global.cart)
        this.computeTotalAmount(product)
        this.events.publish('addedToCart', this.global.cart)
        this.presentToast(`${product.name} is added in the cart`)
      } else {
        console.log('already in cart');
        this.presentToast('Already added in the cart')
      }

      console.log(this.global.cart);

    } catch (error) {
      console.log(error);
      this.alertMessage.open('Error Occurred', 'Please try again.')
    }

  }



  async changeNumberOfOrder(product) {

    // const res: any = await this.storage.get('cart')
    if (this.global.cart) {
      for (let i = 0; i < this.global.cart.length; i++) {
        if (this.global.cart.id === product.id) {
          this.global.cart.push(product)
          this.storage.set('cart', this.global.cart)
          this.events.publish('addedToCart', this.global.cart)
        }
      }
    } else {
      this.global.cart.push(product)
      this.storage.set('cart', this.global.cart)
      this.events.publish('addedToCart', this.global.cart)
    }
    console.log(this.global.cart);
    this.storage.set('cart', this.global.cart)
  }

  async computeTotalAmount(product) {
    const amt = await this.storage.get('totalAmount')
    this.global.totalAmount = (parseInt(product.price) * product.number_product) + amt
    this.displayAmount = this.global.totalAmount
    this.storage.set('totalAmount', this.global.totalAmount)
  }

  async getGlobalAmount() {
    const amt = await this.storage.get('totalAmount')
    this.displayAmount = amt
  }


  async doInfinite(infiniteScroll) {
    this.count++
    console.log('Begin async operation');

    setTimeout(async () => {
      const res: any = this.searchWord === '' ? await this.api.getProductsPerCategory(this.categoryId, this.count) : await this.api.searchProductPerCategory(this.searchWord, this.categoryId, this.count)
      res.map(data => { data.number_product = 1 })
      res.map(product => {
        product.meta_data.map(data => {
          if (data.key === '_woo_uom_input') {
            product.unit = data
          }
        })
        this.products.push(product)
      })
      console.log(this.products);
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1000);
  }

  async onInput() {
    console.log(this.searchWord);
    try {
      this.loadingController()
      const res: any = await this.api.searchProductPerCategory(this.searchWord, this.categoryId, 1)
      res.map(data => { data.number_product = 1 })
      res.map(product => {
        product.meta_data.map(data => {
          if (data.key === '_woo_uom_input') {
            product.unit = data
          }
        })
      })
      this.loading.dismiss()
      this.products = res;
      console.log(res);
    } catch (error) {
      this.loading.dismiss()
      console.log(error);
    }
  }

  loadingController() {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: ` <div class="custom-spinner-container">
                 <img src="/assets/imgs/icons/spinner.svg" width="200px" height="100px">
                 </div>
                 <p class="text-center font-15">Please wait</p>`
    });

    this.loading.present();
  }


  presentToast(msg) {
    this.toast.showWithOptions({
      message: msg,
      duration: 3000,
      position: "bottom",
      styling: {
        backgroundColor: '#1E1E1F',
        textColor: '#FCFCFF',
      }
    }).subscribe(toast => { console.log(toast); });
  }

  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    this.count = 1
    this.getProducts()
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  presentAlertServerError() {
    let alert = this.alertCtrl.create({
      title: 'Server Error!',
      subTitle: 'Pasensya kababayan, Paki-ulit nalang ang pag-bukas ng app.',
      buttons: [{
        text: 'Refresh',
        handler: () => {
          this.getProducts()
        }
      },
      ]
    });
    alert.present();
  }

}
