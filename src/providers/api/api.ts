import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GlobalsProvider } from '../globals/globals';
import { Storage } from '@ionic/storage';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  authUrl = 'https://palengkesentral.com/wp-json/jwt-auth/v1'
  headers: any;

  constructor(
    public http: HttpClient,
    private globals: GlobalsProvider,
    private storage: Storage,
  ) {


  }


  // Wordpress

  getMediaAnnouncement(id) {
    return this.http.get(`${this.globals.wordPressBaseUrl}/media/${id}`).toPromise()
  }

  getAnnouncement() {
    return this.http.get(`${this.globals.wordPressBaseUrl}/posts`).toPromise()
  }

  getSpecificPage(slug) {
    return this.http.get(`${this.globals.wordPressBaseUrl}/pages?slug=${slug}`).toPromise()
  }

  // Products

  getAllProducts(featured?) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/products?status=publish&featured=${featured}&consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  getAllRelatedProducts(ids) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/products?include=${ids}status=publish&consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  getProductsPerCategory(id, page_number) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/products?category=${id}&status=publish&page=${page_number}&consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  getAllCategories() {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/products/categories?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  searchProduct(word) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/products?search=${word}&status=publish&consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  searchProductPerCategory(word, id, page_number) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/products?search=${word}&status=publish&category=${id}&page=${page_number}&consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  getBestSeller() {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/reports/top_sellers?period=month&page=1&consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  getSpecificProduct(id) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/products/${id}?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  // Customer

  async getSpecificCustomer(id) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/customers/${id}?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  async updateUser(id, data) {
    return this.http.post(`${this.globals.wooCommerceBaseUrl}/customers/${id}?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`, data).toPromise()
  }

  // Order

  getPaymentGateway() {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/payment_gateways?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  createOrder(data) {
    return this.http.post(`${this.globals.wooCommerceBaseUrl}/orders?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`, data).toPromise()
  }

  getOrderByCustomer(id, status) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/orders?customer=${id}&status=${status}&consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  getShippingFee(zone, method) {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/shipping/zones/${zone}/methods/${method}?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  getCoupons() {
    return this.http.get(`${this.globals.wooCommerceBaseUrl}/coupons?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`).toPromise()
  }

  // Authentication 

  registerUser(data) {
    return this.http.post(`${this.globals.wooCommerceBaseUrl}/customers?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`, data).toPromise()
  }

  loginUser(data) {
    return this.http.post(`${this.authUrl}/token`, data).toPromise()
  }

  async getMe() {
    return this.http.get(`${this.globals.wordPressBaseUrl}/users/me?consumer_key=${this.globals.consumer_key}&consumer_secret=${this.globals.consumer_secret}`, { headers: { Authorization: `Bearer ${await this.storage.get('token')}`, 'Content-type': 'application/json' } }).toPromise()
  }

  facebookLogin(fbToken) {
    return this.http.post(`${this.globals.wordPressBaseUrl}/m_facebook/login?token=${fbToken}`, {}).toPromise()
  }

  // Address API

  getAllProvinces() {
    return this.http.get(`assets/ph/provinces.json`).toPromise()
  }

  getAllCities() {
    return this.http.get(`assets/ph/cities.json`).toPromise()
  }


  // async getMe() {
  //   try {
  //     this.https.setDataSerializer('json')
  //     const res: any = await this.https.get(`${this.globals.wordPressBaseUrl}/users/me`, {}, { Authorization: `Bearer ${await this.storage.get('token')}`, 'Content-type': 'application/json' })
  //     const data = {
  //       data: JSON.parse(res.data),
  //       result: res.status
  //     }
  //     console.log(data);
  //     return data
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }


}
