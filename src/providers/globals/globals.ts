import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the GlobalsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalsProvider {

  wordPressBaseUrl: string = 'https://palengkesentral.com/wp-json/wp/v2';
  wooCommerceBaseUrl: string = 'https://palengkesentral.com/wp-json/wc/v3'

  consumer_key = 'ck_5f037448a557946008ed43a1e2bf13a51dc96d4e'
  consumer_secret = 'cs_a7a516fa677169d493ff0b84697940d5ac318eee'

  public cart: any = []
  public totalAmount: number


  constructor(public http: HttpClient) {
    console.log('Hello GlobalsProvider Provider');
  }


}
