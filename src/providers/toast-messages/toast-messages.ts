import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Toast, ToastController, ToastOptions } from 'ionic-angular';

/*
  Generated class for the ToastMessagesProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ToastMessagesProvider {

  connection: Toast;

  constructor(
    private toastCtrl: ToastController
    ) { }

  presentToast(payload: ToastOptions, callback: VoidFunction = this.defaultCallback) { // Your toast message
    const toast = this.toastCtrl.create({
      message: payload.message || 'Toast test',
      duration: payload.duration || 3000, // Specify your duration by default it is always set to 3000
      position: payload.position || 'bottom', // Position of your toast by default it is always on bottom
      showCloseButton: payload.showCloseButton || false,
      ...payload
    });

    toast.onDidDismiss(() => {
      callback()
    });

    toast.present();
  }


  defaultCallback() {
    console.log('Your callback function')
  }


  detectConnection(message, duration, style) { // Toast only for internet events
    this.connection = this.toastCtrl.create({
      message: message,
      position: 'bottom',
      duration: duration,
      cssClass: style
    });
    this.connection.present();
    this.connection.onDidDismiss(() => { this.connection = undefined })
  }


  dismissConnectionToast() { // Dismiss the network toast event
    this.connection.dismiss()
  }




}
