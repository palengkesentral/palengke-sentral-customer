import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, AlertOptions } from 'ionic-angular';

/*
  Generated class for the AlertMessageProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AlertMessageProvider {

  constructor(
    public http: HttpClient,
    private alertCtrl: AlertController
    ) {
  }

  open(title: string, message: string, buttonsArray: Array<String> = ['OK'], addParams: AlertOptions = {}) {
    return new Promise(resolve => {
      const buttons: Array<object> = buttonsArray.map(text => {
        const data = {
          text,
          handler: () => resolve(text)
        }
        return data
      })

      const confirm = this.alertCtrl.create({
        title: title,
        message: message,
        buttons,
        enableBackdropDismiss: false,
        ...addParams
      });

      confirm.present();
    })
  }

}
