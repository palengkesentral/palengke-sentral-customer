import { Component } from '@angular/core';

/**
 * Generated class for the PlaceholderLoaderComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'placeholder-loader',
  templateUrl: 'placeholder-loader.html'
})
export class PlaceholderLoaderComponent {

  text: string;

  constructor() {
    console.log('Hello PlaceholderLoaderComponent Component');
    this.text = 'Hello World';
  }

}
