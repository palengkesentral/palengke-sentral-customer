import { NgModule } from '@angular/core';
import { PlaceholderLoaderComponent } from './placeholder-loader/placeholder-loader';
@NgModule({
	declarations: [PlaceholderLoaderComponent],
	imports: [],
	exports: [PlaceholderLoaderComponent]
})
export class ComponentsModule {}
